﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Framework.UI;

namespace Com.Inwinic.ChassisSystemByWire
{
	public class UIManager : UICanvasBase
	{
		#region Public Variables
		public CameraViewControl CarCamera;

		public Button LookByCarBack;
		public Button LookByCarIn;
		public Button LookByCarLeft;
		#endregion

		#region Private Variables

		#endregion

		#region MonoBehaviour CallBacks
		// Start is called before the first frame update
		void Start()
		{
            if (!CarCamera)
            {
				CarCamera = Camera.main.GetComponent<CameraViewControl>();
            }
        }

		// Update is called once per frame
		void Update()
		{

		}


        private void OnEnable()
        {
			LookByCarBack.onClick.AddListener(OnLookByCarBack);
			LookByCarIn.onClick.AddListener(OnLookByCarIn);
			LookByCarLeft.onClick.AddListener(OnLookByCarLeft);
        }

        private void OnDisable()
        {
			LookByCarBack.onClick.RemoveListener(OnLookByCarBack);
			LookByCarIn.onClick.RemoveListener(OnLookByCarIn);
			LookByCarLeft.onClick.RemoveListener(OnLookByCarLeft);
		}

        #endregion

        private void OnLookByCarBack()
        {
			CarCamera.CurrentPos = CameraViewControl.ECameraPos.CarBack;
        }

		private void OnLookByCarIn()
        {
			CarCamera.CurrentPos = CameraViewControl.ECameraPos.CarIn;
		}

		private void OnLookByCarLeft()
        {
			CarCamera.CurrentPos = CameraViewControl.ECameraPos.CarLeft;
		}
	}
}
