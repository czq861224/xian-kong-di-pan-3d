﻿using Framework.Navigation;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 汽车控制
/// </summary>
public class CarController : MonoBehaviour
{
    /// <summary>
    /// 汽车
    /// </summary>
    public AnyCarAI TheCar;
    /// <summary>
    /// 路径根节点
    /// </summary>
    public Transform Way;
    /// <summary>
    /// 行驶路径
    /// </summary>
    public WaypointsPath Path;

    public GameObject TargetSphere;

    public GameObject SteerWheel;
    /// <summary>
    /// 单例
    /// </summary>
    public static CarController Instance => _instance;

    private static CarController _instance;

    private Rigidbody carRg;
    private Queue<Vector3> _queue = new Queue<Vector3>();
    private Vector3 targetAngle;
    private void Awake()
    {
        _instance = this;
        TheCar.isDriving = false;
        InitPath();
        TheCar.AIcircuit = Path;
        NavigationMagic.Init(Way.GetComponentsInChildren<WayPoint>());

        carRg = TheCar.GetComponent<Rigidbody>();

    }

    private void Update()
    {
        carRg.drag = TheCar.isDriving ? 0.1f : 1f;
        /*        Vector3 angle = new Vector3(0, 0, -TheCar.frontLeftCol.GetComponent<WheelCollider>().steerAngle * 18.5f);
                Quaternion curRotate = SteerWheel.transform.localRotation;
                curRotate.eulerAngles = angle;
                SteerWheel.transform.localRotation = curRotate;*/
        float steerAngle = -TheCar.frontLeftCol.GetComponent<WheelCollider>().steerAngle * 18.5f;
        Vector3 tempAngle = new Vector3(0, 0, steerAngle);
        _queue.Enqueue(tempAngle);
        if (!SteerWheel.transform.localEulerAngles.Equals(targetAngle))
        {
            SteerWheel.transform.localEulerAngles = Vector3.Lerp(SteerWheel.transform.localEulerAngles, targetAngle, 0.5f);
            if (Vector3.Distance(SteerWheel.transform.localEulerAngles, targetAngle) <= 1f)
            {
                SteerWheel.transform.localEulerAngles = targetAngle;
                if(_queue.Count > 0)
                {
                    targetAngle = _queue.Dequeue();
                }
            }
        }
        else
        {
            if (_queue.Count > 0)
            {
                targetAngle = _queue.Dequeue();
            }
        }
       
        //Quaternion curRotate = Quaternion.Euler(SteerWheel.transform.eulerAngles.x, SteerWheel.transform.eulerAngles.y, -TheCar.frontLeftCol.GetComponent<WheelCollider>().steerAngle * 18.5f);
        //SteerWheel.transform.rotation = Quaternion.Lerp(SteerWheel.transform.rotation, curRotate, 0.8f);
    }

    private void InitPath()
    {
        if (Path != null)
        {
            if (Path.transform.childCount > 0)
            {
                for (int i = Path.transform.childCount - 1; i >= 0; i--)
                {
                    Destroy(Path.transform.GetChild(i).gameObject);
                }
            }
            GameObject go = new GameObject("WayPoint_0");
            go.transform.SetParent(Path.transform);
            go.transform.position = TheCar.transform.position;
            Path.Refresh();
        }
    }

    public void Move(Vector3 targetPos)
    {
        WayPoint start = NavigationMagic.FindStartPoint(TheCar.transform.position);
        WayPoint target = NavigationMagic.FindTargetPoint(targetPos);
        if (Path != null)
        {
            if (Path.transform.childCount > 0)
            {
                for (int i = Path.transform.childCount - 1; i >= 0; i--)
                {
                    DestroyImmediate(Path.transform.GetChild(i).gameObject);
                }
            }
            WayPoint[] ways = NavigationMagic.FindWay(start, target);
            Debug.Log(NavigationMagic.ToString(ways));
            if (ways != null && ways.Length > 0)
            {
                for (int i = 0; i < ways.Length; i++)
                {
                    GameObject go = new GameObject("WayPoint_" + i);
                    go.transform.SetParent(Path.transform);
                    go.transform.position = ways[i].transform.position;
                }
                Path.Refresh();
                TheCar.carAIWaypointTracker.Reset();
                TheCar.isDriving = true;
                TargetSphere.transform.position = new Vector3(target.transform.position.x, TargetSphere.transform.position.y, target.transform.position.z);
                TargetSphere.gameObject.SetActive(true);
                TheCar.carAIWaypointTracker.onComplete += () =>
                {
                    TheCar.isDriving = false;
                    TargetSphere.gameObject.SetActive(false);
                };
            }
        }
        else
        {
            Debug.LogError("找不到路径！");
        }
    }
}
