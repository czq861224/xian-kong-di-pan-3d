﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraViewControl : MonoBehaviour
{
    public enum ECameraPos
    {
        CarBack = 0,
        CarIn = 1,
        CarLeft = 2
    }

    public ECameraPos CurrentPos;

    public GameObject CarBackPos;
    public GameObject CarInPos;
    public GameObject CarLeftPos;

    private float CarBackPos_Y;
    private float CarBackTarget_Y;

    private float CarInPos_Y;
    private float CarInTarget_Y;

    private float CarLeftPos_Y;
    private float CarLeftTarget_Y;
    // Start is called before the first frame update
    void Start()
    {
        CarBackPos_Y = CarBackPos.transform.position.y;
        CarBackTarget_Y = CarBackPos.transform.GetChild(0).position.y;

        CarInPos_Y = CarInPos.transform.position.y;
        CarInTarget_Y = CarInPos.transform.GetChild(0).position.y;

        CarLeftPos_Y = CarLeftPos.transform.position.y;
        CarLeftTarget_Y = CarLeftPos.transform.GetChild(0).position.y;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPos;
        switch (CurrentPos)
        {
            case ECameraPos.CarBack:
                this.transform.position = new Vector3(CarBackPos.transform.position.x, CarBackPos_Y, CarBackPos.transform.position.z);
                targetPos = CarBackPos.transform.GetChild(0).position;
                this.transform.LookAt(new Vector3(targetPos.x, CarBackTarget_Y, targetPos.z));
                break;
            case ECameraPos.CarIn:
                /*this.transform.position = new Vector3(CarInPos.transform.position.x, CarInPos_Y, CarInPos.transform.position.z);
                targetPos = CarInPos.transform.GetChild(0).position;
                this.transform.LookAt(new Vector3(targetPos.x, CarInTarget_Y, targetPos.z));*/

                this.transform.position = CarInPos.transform.position;
                this.transform.LookAt(CarInPos.transform.GetChild(0));
                break;
            case ECameraPos.CarLeft:
                this.transform.position = new Vector3(CarLeftPos.transform.position.x, CarLeftPos_Y, CarLeftPos.transform.position.z);
                targetPos = CarLeftPos.transform.GetChild(0).position;
                this.transform.LookAt(new Vector3(targetPos.x, CarLeftTarget_Y, targetPos.z));
                break;
        }   
    }
}
