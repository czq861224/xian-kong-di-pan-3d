﻿/*
 * by YuLei
 * time 2021/7/13
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.Inwinic.ChassisSystemByWire
{
	[RequireComponent(typeof(RawImage))]
	public class MiniMapControl : MonoBehaviour,ICanvasRaycastFilter
	{

		#region Public Variables
		public UIManager uiManager;
		public Camera MiniCamera;
		#endregion

		#region Private Variables
		private RectTransform rectTransform;
		private RawImage rawImage;
        #endregion

        #region MonoBehaviour CallBacks
        // Start is called before the first frame update
        void Start()
		{
			rectTransform = this.transform as RectTransform;
			rawImage = this.GetComponent<RawImage>();
		}

		// Update is called once per frame
		void Update()
		{
			
		}

		#endregion

		#region Public Methods

		public bool IsRaycastLocationValid(Vector2 sp, Camera eventCamera)
		{
			if (Input.GetMouseButtonDown(0))
			{
				Vector2 centerPos = sp - new Vector2(this.transform.position.x, this.transform.position.y);
				var rawPos = uiManager.GetRawScreenPos(centerPos);
				// Debug.LogFormat("ydd-- 图片坐标：{0} 世界坐标：{1}", rawPos, ImagePosToWorldPos(rawPos));
				Vector3 targetPos = ImagePosToWorldPos(rawPos);
				CarController.Instance.Move(targetPos);
			}
			return true;
		}
		/// <summary>
		/// 图片坐标转世界坐标
		/// </summary>
		/// <param name="rawPos"></param>
		/// <returns></returns>
		public Vector3 ImagePosToWorldPos(Vector2 rawPos)
        {
			// 图片大小
			var picSize = rectTransform.sizeDelta;
			// Texture 尺寸
			var texSize = new Vector2(rawImage.mainTexture.width,rawImage.mainTexture.height);
			var texPos = rawPos / (picSize / 2) * (texSize / 2);
			// 点的原始坐标/图片高度的一般*矩阵相机的缩放尺寸
			Vector2 Pos = texPos / (rawImage.mainTexture.height / 2 ) * MiniCamera.orthographicSize;
			// Debug.LogFormat("ydd-- 图片实际大小：{0}，图片原始大小：{1}，相机大小：{2}",picSize,texSize,MiniCamera.orthographicSize);
			return new Vector3(Pos.x, 0, Pos.y);
		}
		#endregion
	}
}
